import static org.junit.Assert.*;
import org.junit.Test;

public class test {

	private final MoteurRPN calc = new MoteurRPN();
	@Test
	public void testSingleInput() {
		calc.empiler(5);
		String res = calc.toString();
		String expected = "[5.0]";
		assertEquals(res, expected);
	} 
	
	@Test
	public void testMultiplication() {
		calc.empiler(5);
		calc.empiler(5);
		calc.appliquer(Operation.MULT);
		String res = calc.toString();
		String expected = "[25.0]";
		assertEquals(res, expected);
	}
	
	@Test
	public void testAddition() {
		calc.empiler(5);
		calc.empiler(15);
		calc.appliquer(Operation.PLUS);
		String res = calc.toString();
		String expected = "[20.0]";
		assertEquals(res, expected);
	}
	@Test
	public void testSoustraction() {
		calc.empiler(10);
		calc.empiler(5);
		calc.appliquer(Operation.MOINS);
		String res = calc.toString();
		String expected = "[-5.0]";
		assertEquals(res, expected);
	}
	@Test(expected = divZERO.class)
	public void testDivisionByZero() {
		calc.empiler(0);
		calc.empiler(5);
		calc.appliquer(Operation.DIV);
	}
}
