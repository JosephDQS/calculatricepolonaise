import java.util.Stack;

public class MoteurRPN {
	
private Stack<Double> pile; 

public MoteurRPN()
{
	  this.pile = new Stack<Double>();
}


public void empiler(double op)
{
	  pile.push(op);
}

public void appliquer(Operation op)
{
	  if(pile.size() > 1)
	  {
		  double op1 = pile.pop();
		  double op2 = pile.pop();
		  if(op.GetSigne()=="/" && op2 ==0)
		  {
			  	throw new divZERO();
		  }
		  else 
		  {			 
			  	pile.push(op.eval(op1,op2));
		  }
		  
	  }
	  else 
		  System.out.println("Nombre d'op�rande insuffisant ");
}

public String toString()
{
	  String s = pile.toString();
	  System.out.println(s);
	  return s;
}
}
