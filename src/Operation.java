
public enum Operation {
	PLUS("+"){
	    @Override
	    public double eval(double x, double y) {
	      return x + y;
	    }
	},
	MOINS("-"){ 
		@Override 
		public double eval(double x, double y){
	     return x - y; 
	     } 
	},	  
	MULT("*"){ 
		@Override 
		public double eval(double x, double y) { 
		return x * y; 
		} 
	},	  
	DIV("/"){
		@Override
		public double eval(double x, double y){ 
		return x / y; }
	};
	private final String symbole;
	Operation(String symb)
	{
		this.symbole = symb; 
	}
	public String GetSigne()
	{
		return this.symbole;
	}
	
	

	  public abstract double eval(double x, double y);	
	
	
}
